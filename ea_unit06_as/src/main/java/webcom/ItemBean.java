/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webcom;

import EJB.ItemFacade;
import com.mycompany.l06.Item;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

@Named(value = "itemBean")
@SessionScoped
public class ItemBean implements Serializable {

    @EJB
    private ItemFacade itemFacade;
    private Item newItem;

    public Item getNewItem() {
        return newItem;
    }

    public void setNewItem(Item newItem) {
        this.newItem = newItem;
    }
    
    public ItemBean() {
    }
    
    @PostConstruct
    private void init(){
        newItem = new Item();
    }
    
    public List<Item> findAll(){
        return itemFacade.findAll();
    }
    
    public String addItem(){
        itemFacade.create(newItem);
        newItem = new Item();
        return "";        
    }
    
    public String delItem(Long id){
        this.itemFacade.remove(itemFacade.find(id));
        return "";
    }
    
    public String editItem(Long id){
        this.newItem.id = id;
        return "editUpdate";
    }
    
    public String updateItem(){
        this.itemFacade.edit(newItem);
        newItem = new Item();
        return "index";
    }
    
}
