/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author m317-Terry
 */
@Named(value = "bean")
@RequestScoped
public class Bean {
    
    private String show;

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    /**
     * Creates a new instance of Bean
     */
    public Bean() {
    }
    
    public String info(){
        return show;
    }
}
