/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsf.crud;

import com.jsf.crud.db.operations.databaseOperation2;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author m317-Terry
 */
@ManagedBean
public class schoolBean {
    private int id;
    private String name;    
    private String editSchoolId;
 
    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getEditSchoolId() {
        return editSchoolId;
    }
 
    public void setEditSchoolId(String editSchoolId) {
        this.editSchoolId = editSchoolId;
    }
 
    // Method To Fetch The Existing School List From The Database
    public List schoolListFromDb() {
        return databaseOperation2.getAllSchoolDetails();
    }
 
    // Method To Add New School To The Database
    public String addNewSchool(schoolBean schoolBean) {
        return databaseOperation2.createNewSchool(schoolBean.getName());        
    }
 
    // Method To Delete The School Details From The Database
    public String deleteSchoolById(int schoolId) {      
        return databaseOperation2.deleteSchoolDetails(schoolId);        
    }
 
    // Method To Navigate User To The Edit Details Page And Passing Selecting School Id Variable As A Hidden Value
    public String editSchoolDetailsById() {
        editSchoolId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedSchoolId");     
        return "updateSchool.xhtml";
    }
 
    // Method To Update The School Details In The Database
    public String updateSchoolDetails(schoolBean schoolBean) {
        return databaseOperation2.updateSchoolDetails(Integer.parseInt(schoolBean.getEditSchoolId()), schoolBean.getName());        
    }
}
