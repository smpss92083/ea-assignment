/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsf.crud;

import com.jsf.crud.db.operations.databaseOperation;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author m317-Terry
 */
@ManagedBean
@RequestScoped
public class studentBean {
 
    private int id;  
    private String name;  
    private String email;  
    private String password;  
    private String gender;  
    private String address;
 
    public ArrayList studentsListFromDB;
 
    public int getId() {
        return id;  
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
 
    public String getGender() {
        return gender;
    }
 
    public void setGender(String gender) {
        this.gender = gender;
    }
 
    public String getAddress() {
        return address;
    }
 
    public void setAddress(String address) {
        this.address = address;
    }  
     
    @PostConstruct
    public void init() {
        studentsListFromDB = databaseOperation.getStudentsListFromDB();
    }
 
    public ArrayList studentsList() {
        return studentsListFromDB;
    }
     
    public String saveStudentDetails(studentBean newStudentObj) {
        return databaseOperation.saveStudentDetailsInDB(newStudentObj);
    }
     
    public String editStudentRecord(int studentId) {
        return databaseOperation.editStudentRecordInDB(studentId);
    }
     
    public String updateStudentDetails(studentBean updateStudentObj) {
        return databaseOperation.updateStudentDetailsInDB(updateStudentObj);
    }
     
    public String deleteStudentRecord(int studentId) {
        return databaseOperation.deleteStudentRecordInDB(studentId);
    }
}
