/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsf.calendar;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author m317-Terry
 */
@Named(value = "calendarView")
@ViewScoped
public class calendarView implements Serializable{
    
    private ScheduleModel eventModel;
    
    private ScheduleModel lazyEventModel;
    
    private ScheduleEvent event = new DefaultScheduleEvent();
    
    private boolean showWeekends = true;
    private boolean tooltip = true;
    private boolean allDaySlot = true;
 
    private String timeFormat;
    private String slotDuration="00:30:00";
    private String slotLabelInterval;
    private String scrollTime="06:00:00";
    private String minTime="04:00:00";
    private String maxTime="20:00:00";
    private String locale="en";
    private String timeZone="";
    private String clientTimeZone="local";
    private String columnHeaderFormat="";
    
    @PostConstruct
    public void init() {
        eventModel = new DefaultScheduleModel();
 
        DefaultScheduleEvent event = DefaultScheduleEvent.builder()
                .title("Final of ANN course")
                .startDate(previousDay9Am())
                .endDate(previousDay12Pm())
                .description("Final project debut")
                .overlapAllowed(true)
                .build();
        eventModel.addEvent(event);
 
        event = DefaultScheduleEvent.builder()
                .title("Final of EA course")
                .startDate(today9Am())
                .endDate(today12Pm())
                .description("Final project debut")
                .overlapAllowed(true)
                .build();
        eventModel.addEvent(event);
 
        event = DefaultScheduleEvent.builder()
                .title("Apply a new cable TV")
                .startDate(today3Pm())
                .endDate(today6Pm())
                .description("At home")
                .overlapAllowed(true)
                .build();
        eventModel.addEvent(event);
 
        event = DefaultScheduleEvent.builder()
                .title("Go back to hometown")
                .startDate(theDayAfter3Pm())
                .endDate(fourDaysLater3pm())
                .description("Hsinchu")
                .build();
        eventModel.addEvent(event);
 
        DefaultScheduleEvent scheduleEventAllDay=DefaultScheduleEvent.builder()
                .title("Auntie's birthday (AllDay)")
                .startDate(fourDaysLater0am())
                .endDate(fiveDaysLater0am())
                .description("Let's go to have a cake")
                .allDay(true)
                .build();
        eventModel.addEvent(scheduleEventAllDay);
 
        lazyEventModel = new LazyScheduleModel() {
             
            @Override
            public void loadEvents(LocalDateTime start, LocalDateTime end) {
                for (int i=1; i<=5; i++) {
                    LocalDateTime random = getRandomDateTime(start);
                    addEvent(DefaultScheduleEvent.builder().title("Lazy Event " + i).startDate(random).endDate(random.plusHours(3)).build());
                }
            }
        };
    }
     
    public LocalDateTime getRandomDateTime(LocalDateTime base) {
        LocalDateTime dateTime = base.withMinute(0).withSecond(0).withNano(0);
        return dateTime.plusDays(((int) (Math.random()*30)));
    }
     
 
    public ScheduleModel getEventModel() {
        return eventModel;
    }
     
    public ScheduleModel getLazyEventModel() {
        return lazyEventModel;
    }
 
    private LocalDateTime previousDay9Am() {
        return LocalDateTime.now().minusDays(1).withHour(9).withMinute(0).withSecond(0).withNano(0);
    }
     
    private LocalDateTime previousDay12Pm() {
        return LocalDateTime.now().minusDays(1).withHour(12).withMinute(0).withSecond(0).withNano(0);
    }
     
    private LocalDateTime today9Am() {
        return LocalDateTime.now().withHour(9).withMinute(0).withSecond(0).withNano(0);
    }
     
    private LocalDateTime theDayAfter3Pm() {
        return LocalDateTime.now().plusDays(2).withHour(15).withMinute(0).withSecond(0).withNano(0);
    }
 
    private LocalDateTime today12Pm() {
        return LocalDateTime.now().withHour(12).withMinute(0).withSecond(0).withNano(0);
    }
     
    private LocalDateTime today3Pm() {
        return LocalDateTime.now().plusDays(0).withHour(15).withMinute(0).withSecond(0).withNano(0);
    }
     
    private LocalDateTime today6Pm() {
        return LocalDateTime.now().plusDays(0).withHour(18).withMinute(0).withSecond(0).withNano(0);
    }
     
    private LocalDateTime fourDaysLater3pm() {
        return LocalDateTime.now().plusDays(4).withHour(15).withMinute(0).withSecond(0).withNano(0);
    }
 
    private LocalDateTime fourDaysLater0am() {
        return LocalDateTime.now().plusDays(4).withHour(0).withMinute(0).withSecond(0).withNano(0);
    }
 
    private LocalDateTime fiveDaysLater0am() {
        return LocalDateTime.now().plusDays(4).withHour(0).withMinute(0).withSecond(0).withNano(0);
    }
     
    public LocalDate getInitialDate() {
        return LocalDate.now().plusDays(1);
    }
 
    public ScheduleEvent getEvent() {
        return event;
    }
 
    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }
     
    public void addEvent() {
        if (event.isAllDay()) {
            //see https://github.com/primefaces/primefaces/issues/1164
            if (event.getStartDate().toLocalDate().equals(event.getEndDate().toLocalDate())) {
                event.setEndDate(event.getEndDate().plusDays(1));
            }
        }
 
        if(event.getId() == null)
            eventModel.addEvent(event);
        else
            eventModel.updateEvent(event);
         
        event = new DefaultScheduleEvent();
    }
     
    public void onEventSelect(SelectEvent<ScheduleEvent> selectEvent) {
        event = selectEvent.getObject();
    }
     
    public void onDateSelect(SelectEvent<LocalDateTime> selectEvent) {
        event = DefaultScheduleEvent.builder().startDate(selectEvent.getObject()).endDate(selectEvent.getObject().plusHours(1)).build();
    }
     
    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Delta:" + event.getDeltaAsDuration());
         
        addMessage(message);
    }
     
    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Start-Delta:" + event.getDeltaStartAsDuration() + ", End-Delta: " + event.getDeltaEndAsDuration());
         
        addMessage(message);
    }
     
    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
 
    public boolean isShowWeekends() {
        return showWeekends;
    }
 
    public void setShowWeekends(boolean showWeekends) {
        this.showWeekends = showWeekends;
    }
 
    public boolean isTooltip() {
        return tooltip;
    }
 
    public void setTooltip(boolean tooltip) {
        this.tooltip = tooltip;
    }
 
    public boolean isAllDaySlot() {
        return allDaySlot;
    }
 
    public void setAllDaySlot(boolean allDaySlot) {
        this.allDaySlot = allDaySlot;
    }
 
    public String getTimeFormat() {
        return timeFormat;
    }
 
    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }
 
    public String getSlotDuration() {
        return slotDuration;
    }
 
    public void setSlotDuration(String slotDuration) {
        this.slotDuration = slotDuration;
    }
 
    public String getSlotLabelInterval() {
        return slotLabelInterval;
    }
 
    public void setSlotLabelInterval(String slotLabelInterval) {
        this.slotLabelInterval = slotLabelInterval;
    }
 
    public String getScrollTime() {
        return scrollTime;
    }
 
    public void setScrollTime(String scrollTime) {
        this.scrollTime = scrollTime;
    }
 
    public String getMinTime() {
        return minTime;
    }
 
    public void setMinTime(String minTime) {
        this.minTime = minTime;
    }
 
    public String getMaxTime() {
        return maxTime;
    }
 
    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }
 
    public String getLocale() {
        return locale;
    }
 
    public void setLocale(String locale) {
        this.locale = locale;
    }
 
    public String getTimeZone() {
        return timeZone;
    }
 
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
 
    public String getClientTimeZone() {
        return clientTimeZone;
    }
 
    public void setClientTimeZone(String clientTimeZone) {
        this.clientTimeZone = clientTimeZone;
    }
 
    public String getColumnHeaderFormat() {
        return columnHeaderFormat;
    }
 
    public void setColumnHeaderFormat(String columnHeaderFormat) {
        this.columnHeaderFormat = columnHeaderFormat;
    }

    /**
     * Creates a new instance of calendarView
     */
    public calendarView() {
    }
    
}
