/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsf.authentication;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author m317-Terry
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String validate(){
        String navResult = "";
        System.out.println("Username: " + userName + " & password: " + password);
        if (userName.equalsIgnoreCase("teacher01") && password.equals("teacher01")){
            navResult = "success";
        } else {
            navResult = "failure";
        }
        return navResult;
    }
    
    public LoginBean() {
    }
    
}
