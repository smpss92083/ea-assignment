/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.mavenproject1.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Hashtable;
import java.util.Map;

/**
 *
 * @author m317-Terry
 */
public class LoadFile {
    private Map<String, Map<String, Integer>> map;
    private Path file;

    public void load(Path file) {
        if (Files.notExists(file))
            System.out.printf("The target file is not exist: %s ", file.toString());
        map = new Hashtable<>();
        // get buffer stream
        try (BufferedReader br = Files.newBufferedReader(file)){
            String line = "";
            int counter = 0;
            while ( (line = br.readLine()) != null){
                addToZipMap(line);
                counter++;
            }
            System.out.printf("Number of lines been read: %s", counter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Map<String, Integer>> getZipCodes() {
        return this.map;
    }

    private void addToZipMap(String rawData) {
        String [] fields = rawData.split("\\s+");
        if (fields.length != 3) {
            System.out.println("Cannot split the raw data: " + rawData);
            return;
        }

        // get county, area, and zip_code
        String county = fields[0];
        String district = fields[1];
        Integer zip_code = Integer.valueOf(fields[2]);

        // get the areas for a county
        if (map.containsKey(county)){
            map.get(county).put(district, zip_code);
        } else {
            Map<String, Integer> dist_zipCode = new Hashtable<>();
            dist_zipCode.put(district, zip_code);
            map.put(county, dist_zipCode);
        }
    }
    
}
