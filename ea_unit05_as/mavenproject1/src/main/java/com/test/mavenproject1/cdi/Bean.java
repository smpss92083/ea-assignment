/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.mavenproject1.cdi;

import com.test.mavenproject1.ejb.Interface;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author m317-Terry
 */
@Named(value = "bean")
@ViewScoped
public class Bean implements Serializable{
    
    private String county;
    private String district;
    private Map<String, Map<String, Integer>> map;

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Map<String, Map<String, Integer>> getMap() {
        return map;
    }

    public void setMap(Map<String, Map<String, Integer>> map) {
        this.map = map;
    }

    @PostConstruct
    private void init(){
        this.county = "臺北市";
        this.district = "中正區";
    }
    
    @EJB(beanName = "Interface")
    private Interface Ejb;
    
    public List<String> getAllCounties() {
        return Ejb.getAllCounties();
    }
    public List<String> getAllDistricts() {
        return Ejb.getAllDistricts(county);
    }
    public int lookup() {
        return Ejb.lookup(county , district);
    }
    
    public Bean() {
    }
    
}
