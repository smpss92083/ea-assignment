/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.mavenproject1.ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author m317-Terry
 */
@Local
public interface Interface {    
    int lookup(String county,String district);
    List<String>getAllCounties();
    List<String>getAllDistricts(String county);
}
