/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.mavenproject1.ejb;

import com.test.mavenproject1.java.LoadFile;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author m317-Terry
 */
@Stateless(name="Interface")
public class Ejb implements Interface{
    
    private Map<String, Map<String,Integer>> map;
    
    @PostConstruct
    public void init(){
        LoadFile loadFile = new LoadFile();
        
        Path zipFilePath = null;
        
        try {
            zipFilePath = Paths.get(getClass().getClassLoader().getResource("./taiwan_zip_codes.txt").toURI());
            loadFile.load(zipFilePath);
            this.map = loadFile.getZipCodes();
            
        } catch (URISyntaxException ex) {
            Logger.getLogger(Ejb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int lookup(String county, String district) {
        int code =0;
        if(map.containsKey(county) && map.containsKey(county)){
            code = map.get(county).get(district);
        }
        return code;
    }

    @Override
    public List<String> getAllCounties() {
        return new ArrayList<String>(map.keySet());
    }

    @Override
    public List<String> getAllDistricts(String county) {
        if(map.containsKey(county)){
            return new ArrayList<String>(map.get(county).keySet());
        }else{
            return null;
        }
    }
    
}
