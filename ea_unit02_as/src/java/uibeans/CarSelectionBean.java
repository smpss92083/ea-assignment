/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uibeans;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import repository.Car;
import repository.CarRepositoryBean;

@Named(value = "carSelectionBean")
@SessionScoped
public class CarSelectionBean implements Serializable{
    
    private String selectedCarID;
    private String name;
    private String manufacturer;
    private String img_uri;
    private List<Car> car;

    public String getName() {
        return name;
    }

    public List<Car> getCar() {
        this.car = repository.findAll();
        return car;
    }

    public void setCar(List<Car> car) {
        this.car = car;
    }

    public void setName(String carName) {
        this.name = carName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String carManufacturer) {
        this.manufacturer = carManufacturer;
    }

    public String getImg() {
        return img_uri;
    }

    public void setImg(String carImg) {
        this.img_uri = carImg;
    }
    

    public String getSelectedCarID() {
        return selectedCarID;
    }

    public void setSelectedCarID(String selectedCarID) {
        this.selectedCarID = selectedCarID;
    }

    
    // Inject the car repository
    // DO NOT REMOVE THE CODE
    @EJB CarRepositoryBean repository;
   
    /**
     * Action method to redirect to displayInfo facelets page.
     * @return target page name
     */
    public String dispalyInfo(){    
        this.name = repository.query(Integer.parseInt(selectedCarID)).getName();
        this.manufacturer = repository.query(Integer.parseInt(selectedCarID)).getManufacturer();
        this.img_uri = repository.query(Integer.parseInt(selectedCarID)).getImg_uri();
        
        return "displayInfo?faces-redirect=true";
        
    }    
    
    
}
